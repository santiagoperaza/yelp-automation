### This repository contains the solution fro Yelp Automation exercise.

### Installation

Before running the test you will need to download all the dependencies by running the below command:

  `npm install`

### Run Some Sample Tests

To execute the entire test suite in local development, you can use any one of the options mentioned below

Option 1: `npm run test`

##### Allure

To generate and view an allure report locally, run `npm run allure-report`.